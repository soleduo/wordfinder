﻿using System;

namespace AgateTest
{
    public class ReadDirection{

        public int readX;
        public int readY;

        public ReadDirection(int x, int y)
        {
            readX = x;
            readY = y;
        }

        
    }


    public class WordFinder
    {
        char[][] boardRef;

        static ReadDirection[] readDirection =
            {
                new ReadDirection(1,0), //Read Left to Right
                new ReadDirection(0, -1), //Read Top to Bottom
                new ReadDirection(-1,0), //Read Right to Left
                new ReadDirection(0,1) //Read Bottom to Top
            };

        public string FindWord(char[][] board, string word)
        {
            boardRef = board;
            char[] wordToChar = word.ToCharArray();
            int wordLength = wordToChar.Length;
            string direction;

            //Find initial character in board
            for (int x = 0; x < board.Length; x++) //for horizontal
            {
                for (int y = 0; y < board[x].Length; y++) //for vertical
                {
                    
                    if (wordToChar[0] == board[y][x]) //read Condition
                    {
                        //Try Read in every direction
                        if (ReadEveryDirection(wordToChar, x, y) != null) //if read completed
                        {
                            var dirTemp = ReadEveryDirection(wordToChar, x, y); //save to temporary variable
                            direction = dirTemp; //fetch direction index from readDirection array
                            string result = ("Column: " + (x + 1).ToString() + ". Row: " + (y + 1).ToString() + ". Direction: " + direction + ". Length:" + wordLength.ToString() + ".");
                            

                            return result;
                        }
                        
                    }
                    continue;
                }
                continue;
            }

            return null;
        }

        string ReadEveryDirection(char[] wordRef, int x, int y)
        {
            for(int dir = 0; dir < readDirection.Length; dir++) //Read at direction stated in readDirection
            {
                ReadDirection reference = TryRead(wordRef, x, y, dir);
                if(reference != null)
                {
                    return dir.ToString();
                }
            }

            return null;
        }

        ReadDirection TryRead(char[] wordRef, int x, int y, int direction)
        {
            //Stop trying if out of board range
            if (x < 0 || y < 0 || x >= boardRef.Length || y >= boardRef[0].Length)
                return null;

            //stop if current character not match the word reference
            if (boardRef[y][x] != wordRef[0])
                return null;

            //return direction if read completed
            if (wordRef.Length == 1)
                return new ReadDirection(readDirection[direction].readX, readDirection[direction].readY);

            //try read next character
            char[] nextCharacter = new char[wordRef.Length - 1];
            Array.Copy(wordRef, 1, nextCharacter, 0, wordRef.Length - 1);
            return TryRead(nextCharacter, x + readDirection[direction].readX, y + readDirection[direction].readY, direction);

        }
    }
}
