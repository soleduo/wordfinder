﻿using System;
using AgateTest;

namespace AgateTest
{
    class MainApp
    {
        static WordFinder wordFinder = new WordFinder();

        static char[][] board =
        {
            new char[] {'A','B','S','C','D','E'}, //Top - Bot = FADE
            new char[] {'F','G','S','G','N','G'}, //Left - Right = DOG
            new char[] {'A','D','O','H','E','F'}, //Bot - Top = HEN
            new char[] {'D','D','I','G','H','H'}, //Right - Left = PIG
            new char[] {'E','H','D','O','G','E'},
            new char[] {'X','V','H','G','I','P'}
        };

        static void Main(string[] args)
        {
            for (int i = 0; i < board.Length; i++) {
                string boardRef = new string(board[i]);
                Console.WriteLine(boardRef);
            }

            

            Console.WriteLine( "\n" + "Search for: ");
            string word = Console.ReadLine();

               string result = wordFinder.FindWord(board, word);
               if (result != null)
               {
                   Console.WriteLine("\n" + "Your word: " + word + " is found at: " + result);
               } else
               {
                   Console.WriteLine("\n" + "Your word not found.");
               }

            Console.WriteLine("\n\n Press any key to quit");
            Console.ReadKey();
        }
    }
}
